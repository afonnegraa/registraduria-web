import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    person: null
  },
  mutations: {
    SET_PERSON: (state, newValue) => {
      state.person = newValue
    }
  },
  actions: {
    setPerson: ({ commit, state }, newValue) => {
      commit('SET_PERSON', newValue)
      return state.person
    }
  },
  plugins: [createPersistedState()]
})
