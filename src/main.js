import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import firebase from 'firebase';

Vue.config.productionTip = false;

var firebaseConfig = {
  apiKey: "AIzaSyBDFvY0VCP9tUzSKnODkcaNW4rktqzwjkg",
  authDomain: "registraduria-457d8.firebaseapp.com",
  databaseURL: "https://registraduria-457d8.firebaseio.com",
  projectId: "registraduria-457d8",
  storageBucket: "registraduria-457d8.appspot.com",
  messagingSenderId: "828246131735",
  appId: "1:828246131735:web:aaa33cd597488e081dc852"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
