import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        dark: true,
        themes: {
            dark: {
                primary: '#FFFFFF',
                accent: '#1E88E5'
            }
        }
    },
    lang: {
        current: 'es'
    }
});
