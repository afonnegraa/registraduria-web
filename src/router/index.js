import Vue from 'vue'
//import store from '@/store';
import VueRouter from 'vue-router'
//SESIONES INI
import Sessions from '../views/sessions.vue'
import Login from '../components/sessions/login.vue'
//SESIONES FIN

//DASHBOARD INI
import Dashboard from '../views/dashboard.vue'
//DASHBOARD FIN

import CiudadanosList from '../components/ciudadanos/ciudadanos_list.vue'

import CiudadanosAdd from '../components/ciudadanos/ciudadanos_add.vue'
import CiudadanosEdit from '../components/ciudadanos/ciudadanos_edit.vue'
import CiudadanosView from '../components/ciudadanos/ciudadanos_view.vue'

Vue.use(VueRouter);


const routes = [
  {
    path: '/',
    component: Sessions,
    children: [
      {
        path: '',
        name: 'Ingreso',
        component: Login
      },      
    ]
  },
  {
    path: '/dashboard',
    component: Dashboard,
    children: [
      {
        path: '',
        name: 'CiudadanoList',
        component: CiudadanosList,
      },
      {
        path: 'add',
        name: 'CiudadanoAdd',
        component: CiudadanosAdd,
      },
      {
        path: 'edit',
        name: 'CiudadanoEdit',
        component: CiudadanosEdit,
      },
      {
        path: 'view',
        name: 'CiudadanoView',
        component: CiudadanosView,
      },
      {
        path: 'reports',
        name: 'Reports',
        component: CiudadanosView,
      },
    ]
  },
]

const router = new VueRouter({
  routes
})

export default router
